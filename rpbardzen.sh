#!/bin/bash

 source /home/marulo/.rpvar

 killbar() {
    killall dzen2
}

 bar() {
    checkbar=$(pgrep dzen2 | wc -l)
    if [ "$checkbar" != "0" ]; then
	$(killbar)

     else
	rp_statdzen.sh | dzen2 -ta l -h 20 -bg $rpcolor_0 -fg $rpcolor_1 -fn 'JetBrainsMono nerd font-14' &

     fi
}

 BARPADDING=$(ratpoison -c set | grep ^padding | cut -d ' ' -f 3)
if [ "$BARPADDING" == "0" ]; then
    ratpoison -c "set padding $rp_bar_padding"
    $(bar)
    
elif [ "$BARPADDING" == "20" ]; then
    $(bar)
    ratpoison -c "set padding $rp_padding"
    
else
    $(bar)

 fi
